var myString = "How long am I?";
var myString2 = "The Rock is the greatest WWE warrior of all time.";

var message = "IMPORTANT MESSAGE:\n\nError detected!\nPlease check your data";
// \n is a character much like in python that break a line and creates a new space. Use as many times as needed.
// Here are a few other Common Escape Sequences:
/*

\t       mean its a tab or 4 spaces. Compatible with Python. 
\n       which breaks into a new line
\"       which gives you a double quote
\'       Single quote or apostraphe
\\       The backslash itself 
\x99     Two-digit number specifying the hexidecimal value of an ASCII character
\u9999   Four Digit hexidecimal number specifying a Unicode character 

*/  

// Methods of a String object, although these may be more simplified to write since JS has evolved
/*
concat()        joins strings together to create a new string 
indexOf()       Returns the position of the first occurence of a specified value in a string
lastIndexOf()   Returns the position of the last occurence of a specified value in a string
repeat()        Returns a string with a specified number of copies of the string it was called on.
replace()       Searches for a match between a substring and a string, and replaces the substring with a new substring.
split()         Splits a string into an array of substrings, and returns the new array.
substr()        Extracts a substring from a string, beginning at a specified start position, and through the specified 
                number of characters.
toUpperCase() or toUpper() Upper()     converts a string to uppercase
toLowerCase() or toLower() Lower()     converts a string to lowercase
*/

var string1 = "Brock Lesnar is the real deal";
var string2 = "The John Cena nation canot be stopped nor contained";

var longString = string1.concat("<br>" + string2);
string1.indexOf('Brock');

var bigCat = "The jaguar is mighty and majestic.";
var predatorCat = bigCat.repeat(3); 

var huntingCat = bigCat.substr(5, 10);

var purringCat = bigCat.replace("jaguar", "lion");

var string3 = string1.replace("Brock Lesnar", "The Rock Dwayne Johnson!!!!!!!!");

var catPurly = string1.split(" "); 

var sub1 = string2.substr(4,14); 
var sub2 = string3.substr(0, 22);

var sub3 = string3.toLowerCase();
var sub4 = string3.toUpperCase();
var sub5 = purringCat.toUpperCase();

function detectSpam(input) {
    input = input.toLowerCase(); 
    return input.indexOf("fake")
}

var myString = prompt("Enter a string");

var name = "John";
var course = "Mathematics III";
var myString = "Hello ${name}, welcome to ${course}.";

