/*var message = "";
   var bool = true; 
   if(bool) message = "The test condition evatualted to TRUE";

   The book should show then tell, here what it means is if the boolean retuns true, then write the message 
   "The test condition evaluated to TRUE". 

    The basic structure of a for loop goes as such

    if (true or false statement) {
        message here that prints the statement or does the thing we want it to. 
    }

    you could then say:
    if (!bool) message = "The value of bool is FALSE"; 
    
    or written as:
    if(!boolean) {
        message = "The value is False";
    }

    var message = "";
    var temperature = 60;
    if(temperature < 64) message = "Turn on the heating";

JavaScript Comparison Operators

==    Is equal to 
===   Is equal to both value and type
!=    Not equal to 
>     is greater than
<     is less than 
>=    is greater than or equal to 
<=    is less than or equal to  

*/

function detectSpam(input) {
    input = input.toLowerCase();
    if(input.indexOf("fake") < 0) { // indexOf() method is what returns the position of the 
                                    // first occurence of a specified value in a string 
        return false;
    }
    return true;
}

var mystring = prompt("Enter a string"); 
alert(detectSpam(mystring));

/* 
var x = 2; 
if(x == "2");   // returns true as "2" string reads as the number 2
if(x === "2");  // returns false as a string is not numeric, it must eqal both the value and type. 

*/

var x = 4;
var y = 4; 
var fruit1 = "grape";
var fruit2 ="GRAPE";

    if(x === y){
        document.write("That was a smash success");
    }

    document.write("<br>");
    
    if(fruit1 == fruit2){
        document.write("Thats a true statement");
    } else {
        document.write("not so much a true statement"); 
    }

    document.write("<br>");
    
    if(fruit1 = fruit2){
        document.write("Thats a true statement");
    } else {
        document.write("not so much a true statement"); 
    }

    document.write("<br>");

var temperature = 32; 

    if(temperature < 64) {
        message = "Turn on the heating!";
        heatingStatus = "on"; 
        document.write(heatingStatus);
    } else {
        message = "Temperature is high enough";
        heatingStatus = "off";
        document.write(heatingStatus);
    }

document.write("<br>");

var temperature1 = 70
 //errorMessage = count + ((count == 1)? " error ":" error ") + "found.";

if (temperature1 < 64) {
    document.write(message = "turn on the heating!" + "<br>");
    document.write(heatingStatus = "on" + "<br>");
    document.write(fanStatus = "off");
} else if(temperature1 > 72) {
    document.write(message = "Turn on the fan!" + "<br>");
    document.write(heatingStaus = "off" + "<br>");
    document.write(fanStatus = "on");
} else {
    document.write(message = "Temperature is OK" + "<br>");
    document.write(heatingStatus = "off" + "<br>");
    document.write(fanStatus = "off");
}

document.write("<br>");

var food = "ribs";

if(food = "ribs") {
    message = "Thats a fine selection you made!";
    document.write(message);
} else if(food = "hotdogs") {
    message1 = "Hotdogs aren't bad, you just need quality ingredients, which is rare in sausage."
    document.write(message1);
} else {
    message2 = "You have to eat something, a man needs to eat";
    document.write(message2);
}

document.write("<br>");

var color = "purple";

switch(color) {
    case "red" :
        document.write(message = "Stop!");
        break;
    case "yellow" :
        document.write(message = "Pass with caution");
        break;
    case "green" :
        document.write(message = "Come on through");
        break;
    default :
        document.write(message = "Traffic light out of service. Pass only with great care");
}

document.write("<br>");

var color = "green";

switch(color) {
    case "red" :
        document.write(message = "Stop!");
        break;
    case "yellow" :
        document.write(message = "Pass with caution");
        break;
    case "green" :
        document.write(message = "Come on through");
        break;
    default :
        document.write(message = "Traffic light out of service. Pass only with great care");
}
document.write("<br>");

var color = "yellow";

switch(color) {
    case "red" :
        document.write(message = "Stop!");
        break;
    case "yellow" :
        document.write(message = "Pass with caution");
        break;
    case "green" :
        document.write(message = "Come on through");
        break;
    default :
        document.write(message = "Traffic light out of service. Pass only with great care");
}
document.write("<br>");

var color = "red";

switch(color) {
    case "red" :
        document.write(message = "Stop!");
        break;
    case "yellow" :
        document.write(message = "Pass with caution");
        break;
    case "green" :
        document.write(message = "Come on through");
        break;
    default :
        document.write(message = "Traffic light out of service. Pass only with great care");
}
document.write("<br>");

var temperature5 = 100;

if(temperature5 >=64 && temperature5 <= 72) {
    document.write(message = "The temperature is OK");
} else {
    document.write(message = "The tempature is out of range!");
}
document.write("<br>");

var count = 10;
var sum = 0;

    while(count > 0) {
        sum = sum + count;
        count--;
    }
    alert(sum);    
    
document.write("<br>");

var value = 20;
var value1 = 20;
var value2 = 20;
var count = 0;
var count3 = 2;
sum2 = 0; 

while(value <= 40) {
    count = count + value;
    value++;
    console.log(value);
}
document.write(value + "<br>");
document.write(count + "<br>");

while(value1 < 40) {
    count = count + value1;
    value1++;
    console.log(value1);
}
document.write(value1 + "<br>");
document.write(count + "<br>");

/*while(value > 0) {        I had to cancel this out because it generates an infinite loop as 20 is
                            always greater than 0, and the loop counts by 1.
    count = count + value;
    value++;
    console.log(value);
}
document.write(value);*/

document.write("<br>");
while(value2 < 30) {
    count = count + value2;
    value2++;
    console.log(value2);
}
document.write(value2 + "<br>");
document.write(count +"<br>");

do {
    sum1 = count3++;
} while (count3 < 7)
    document.write(sum1);  // here its just 6 because it adds up to less than 7, so 6 and its that 
                           //  way due to the logic set up. That being the do while loop, 
                           // do this thing until condition met.

document.write("<br>");


do {
    sum2 = sum2 + count3++;  // While not clearely stated simply having a variable plus the count3++ 
                             // will return 7 as while sum3 = 0, simply having it defined beforehand 
                             // and adding it on causing the result of 6 to be +1 giving you 7
} while (count3 <= 7)
    document.write(sum2);

document.write("<br>");
/*
for loop syntax, it always follows this pattern

for(x = 0; x < 10; x++;) {
    // Here goes your statements.
}

*/
var count4; 
var sum4 = 0;

for(count4 = 10; count4 > 0; count4--) {
    sum4 = sum4 + count4;
    console.log(count4);
}
document.write(count4 +"<br>");  // count4 returns zero as that is the final result of its countdown from 10
document.write(sum4 + "<br>");            // returns 55 as it added everything in the loop and resulted in 55.

var count5; 
var sum5 = 2;

for(count5 = 50; count5 < 90; count5++) {
    sum5 = sum5 + count5;
    console.log(count5);
}
document.write(count5 +"<br>"); 
document.write(sum4 + "<br>");

var sum = 0;
for(var count = 10; count > 0; count--) {
    sum = sum + count;
}
document.write(sum + "<br>");

var count6 = 10;
var sum6 = 0;

while(count6 > 0) {
    sum6 = sum6 + count6;
    if(sum6 > 42) break; 
    count6--;
}
document.write(sum6);

