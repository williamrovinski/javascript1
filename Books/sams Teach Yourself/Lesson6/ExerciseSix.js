// toString() is a method used to return a number as a character string 

var num = 666;
num.toString();         // returns string "666", either pass a variable connected to it or 

(666).toString();       // returns string "666", pass the parameter instead of the variable


var num2 = 69;
num.toString();

(69).toString();

var x = 13;
//alert(x.toString());    //alerts 13
//alert(x.toString(2));   //alerts 1101
//alert(x.toString(16));  //alerts d

// toFixed() is a method where it returns a string out of an integer but also its decimal places, 
// or as many places as you specify

x.toFixed(0); // returns "666"
x.toFixed(3); // retutns "666.000"

// toExponential() is a method that returns to what decimal place the number would be if it were an exponent

num.toExponential(4);     // returns 6.6600e+2
num.toExponential(6);     // returns 6.660000e+2

// Number. is a constructor for integers, its a constant variable .
// By using a constructor such as Number, we can use this in conjuction:
// .isNaN(), which is a method which tests to see if the argument you entered is returned as an integer or not
// If its a number, it returns false, if its a string then it returns true   

// Number.isInteger() method determines whether a value or expression passed is a number or not, will return true or false.

var now = new Date(); 
var answer = confirm("Do you want to continue?"); // answer will contain either true or false 

var success = false; //correct
var success1 = "false"; //incorrect, a Boolean value of true or false, do not enclose the value in quotation
                        //marks; otherwise, the value will be interpreted as a string literal. You can even 
                        //see this be the color encoding.

function formatNumber() {
    var blandNumber = 12.6969696969; 
    document.write(blandNumber.toFixed(3));
}