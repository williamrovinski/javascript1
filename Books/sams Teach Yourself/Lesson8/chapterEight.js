var myArray = new Array();

var anotherArray = new Array(); 

var someArray = ['Monday', 'Tuesday', 'Wednesday'];

anotherArray[0] = "Blue";
anotherArray[1] = "Red";
anotherArray[2] = "Yellow";
anotherArray[3] = "Purple";
anotherArray[4] = "Green";
anotherArray[5] = "Turquoise";
anotherArray[6] = "Orchid";


myArray[0] = 'Monday' + "<br>";
myArray[1] = 'Tuesday' + "<br>"; 
myArray[2] = 'Wendesday' + "<br>";

// Note if I called someArray in the HTML then we would see two instances of Monday, Tuesday, Wednesday.

//myArray[50] = 'Ice cream Day';

/*
These are Array Methods 

concat()        Joins multiple arrays
join()          Joins all the array elements together in a stromg
toString()      Returns the array as a string 
indexOf()       Searches the array for specific elements
lastIndexOf()   Returns the last item in the array that matches the search criteria
slice()         Returns a new array from the specific index length
sort()          Sorts the array alphabetically or by the supplied function
splice()        Adds or deletes the specified index(es) to or from the array

*/

var myOtherArray = ['Thursday', 'Friday'];
var myWeek = myArray.concat(myOtherArray); 
// myWeek will contain 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'

var longDay = myArray.join();

var longDay = myArray.join("-"); // returns Monday - Tuesday - Wednesday

var longDay2 = anotherArray.toString();

var myWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
var myShortWeek = myWeek.slice(-3, 4);

var myNextWeek = myWeek.slice(1, -2);

var myDays = myWeek.sort();
var myColors = anotherArray.sort();

/*
forEach()        This method of array's accepts a function as an argument and applies it sequentially
                 to every item in an array. Note that the forEach() method returns no actual value.
                 It merely cally a particular function on each element in your array.
*/

var thisArray = [2, 3, 4, 5, 6, 7];
function cube(x) { 
    document.write(x*x*x + "<br>");
    console.log(x*x*x); // This function cube() will show x as each number in the array and then multiply as following  
}

/*
map()        This method is similiar to the forEach() method, however, it will return a new array and is often 
             useful where you want the original array to remain unaltered. 

*/

var basicArray = [2, 3, 4, 5, 6, 7];
function Display(x) {
    return (x*x*x);
}
var newArray = basicArray.map(Display);

var newArray2 = [1, 2, 3, 4, 5, 6, 7];
function fuse(x) {
    console.log(x*x*x);

for (var y of newArray2) {
    cube(y);
}
}

var array1 = ['apple', 'banana', 'pear'];
var array2 = ['orange', ...array1, 'cherry', 'fig'];

var mathArray = [91, 35, 17, 101, 29, 77];
