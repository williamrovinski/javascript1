var answer = confirm("Are you happy to continue?");

var solution = prompt("What is your full name?");

var myDivContents = document.getElementsById("div1").innerHTML;

document.getElementsById("div1").innerHTML = "<p>Here is some new text instead!</p>";

alert("You've visited " + history.length + " web pages in this browser session"); // history object in javascript can be manipulated like anything else

history.go(-3); // go back 3 pages
history.go(2);  // go forward 2 pages 
// for the history object, there are a few methods to manipulate it, 2 of which are history.foward();, history.back(); 

function accessHistory() {
    history.go("amazon.com");
}

// here's the syntax to access the history of a page: [protocol]//[hostname]:[port]/[pathname][search][hash]
function createPortTable() {
    document.write("<table>");
    document.write("<tr><td>appName</td><td>" +navigator.appName + "</td></tr>");   
    document.write("<tr><td>appCodeName</td><td>" + navigator.appVersion + "</td>");
    document.write("<tr><td>appVersion</td><td>"+navigator.language + "</td></tr>");
    document.write("<tr><td>cookieEnabled</td><td>"+navigator.cookieEnabled + "</td></tr>");
    document.write("<tr><td>cpuClass</td><td>"+navigator.cpuClass + "</td></tr>");
    document.write("<tr><td>onLine</td><td>"+navigator.onLine + "</td></tr>");
    document.write("<tr><td>platform</td><td>"+navigator.platform + "</td></tr>");
    document.write("<tr><td>No. of plugins</td><td>"+navigator.plugins.length + "</td></tr>");
    document.write("</table>");
}        

var mydate = new Date();

var year = mydate.getFullYear(); // four digit year so that would be 2018
var month = mydate.getMonth(); // month number 0 - 11; 0 is Jan, etc.
var date = mydate.getDate(); // day of the month 1 -31
var day = mydate.getHours(); // hours part of the time, 0-23
var minutes = mydate.getMinutes(); // minutes part of time, 0 - 59
var seconds = mydate.getSeconds(); // seconds part of time, 0 - 60 

        