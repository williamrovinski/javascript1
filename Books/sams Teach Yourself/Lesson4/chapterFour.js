function addTax(subtotal, taxRate) {
    var total = subtotal * (1 + (taxRate/100));
    return total; 
}

var invoiceValue = addTax(50,10);

var a = 10;
var b = 10;

function showVars() {
    var a = 20; // declare a new local variable 'a'
    b = 20; // Change the value of global variable 'b'
    return "Local variable 'a' = " + a + "\nGlobal variable 'b' = " + b;
}

var message = showVars();

function showVars2() {
    var a = 20; // declare a new local variable 'a'
    b = 20; // Change the value of global variable 'b'
    return "this.a = " + this.a + "\nLocal variable 'a' = " + a + "\nGlobal variable 'b' = " + b;
}
var message2 = showVars2();

function myFunction(x) {
    var y = x;
    if(x > 50) {
        var y = 10; 
        alert("Inner y = " + y); // alerts 10
    }
    alert("Outer y = " + y); // alerts 10 ... more statements ...
}

function myFunction2() {
    const x = 300;
    x = 400; // Error guaranteed at this line

}

// This is a sort of function, it is shorthand and an arrow function. 
var sayHello = function(){
    alert("Hello");
};
//This does exactly what lines 42 and 43 say but is more concise, they are called arrow/lambda functions. 
var sayHello = () => alert('Hello');

// Another way to look at this is to write: param => statements/expression

myFunc = (x, y, z) => {
    let area = x*y + 5;
    let boxes = area / z;
    return boxes;
}

function warm(temp) {
    alert("Warning:\nA Temperature of " + temp + " is too high");
}

function warm2(temp, headline='Warning') { //In this function we set two parameters which we define later in our alert
    alert(headline + ":\nA Temperature of " + temp + " is too high");
}

function calculateTemperature() {
    var temp1 = 60;
    var temp2 = 15;
    cTemp = document.write((temp1 - temp2) * (5/7)); 
}
