/* 
Mouse Events, when using your mouse, these events occur 

onclick()        The user clicks on the element
oncontextmenu()  The user right-clicks on an element to open a context menu
ondblclick()     The user double-clicks on an element
onmousedown()    The user presses a mouse button over an element
onmouseenter()   The pointer is moved onto an element
onmouseleave()   The pointer is moved out of an element
onmousemove()    The pointer is moving while it is over an element
onmouseover()    The pointer is moved onto an element or onto one of its children

Keyboard Events, these are events that happen when you press a button on a keyboard

onkeydown();      The user is pressing a key, key is pressed down.
onkeypress();     The user presses a key, I'm guessing this is like a toggle button.
onkeyup();        The user releases a key, a key has the pressure on the button released

DOM Objects Events

onerror();        An error occurs while loading an external file
onload();         An object has loaded
onresize();       An document view is resized
onscroll();       An element's scrollbar is being scrolled

Form Events

onblur();         An element loses focus
onchange();       The content, selection, or checked state has changed
onfocus();        An element gets focus
onreset();        A form is reset 
onselect();       The user selects some text
onsubmit();       A form is submitted

*/

handlername = "JavaScript code";

var myLink = document.getElementById('a1'); 
function sayHello() {
    alert('hello W3C!');
}
myLink.onclick = sayHello;


