var context;
var x = 50;
var y = 50;
var counter = 0; 

function paint() {
    context.beginPath();
    context.fillStyle="#ff0000";
    context.arc(x, y, 15, 0, Math.PI*2, false);
    context.closePath();
    context.fill();
}

function animate() {
    context.clearRect(0, 0, 400, 300);
    counter++;
    x += 20 * Math.sin(counter);
    y += 20 * Math.cos(counter);
    paint();
}

window.onload = function() {
    context = canvas1.getContext('2d');
    setInterval(animate, 100);
}

/* arc() Method draws a circle and does this with a few parameters

x           The x-coordinate of the center of the circle 
y           The y-coordinate of the center of the circle 
r           The radius of the circle
sAngle      The starting angle, in radians (0 is at the 3 o'clock position of the arc's circle
eAngle      The ending angle, in radians

counterclockwise    Optional, Specifies whether the drawing should be counter clockwise or clockwise. False is default
                    and indicates clockwise, while true indicates counter-clockwisew, its a boolean.

example:

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.beginPath();
ctx.arc(100, 75, 50, 0, 2 * Math.PI);
ctx.stroke();

beginPath() Method begins the sprite path, with additional methods it can also rest or cause the current path to go in other directions 

clearRect() method clears the specified pixels within a given rectangle
x           The x-coordinate of the upper-left corner of the rectangle to clear
y           The y-coordinate of the upper-left corner of the rectangle to clear
width       The width of the rectangle to clear, in pixels
height      The height of the rectangle to clear, in pixels

var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
ctx.fillStyle = "red";
ctx.fillReact(0, 0, 300, 150);
ctx.clearRect(20, 20, 100, 50);

*/



