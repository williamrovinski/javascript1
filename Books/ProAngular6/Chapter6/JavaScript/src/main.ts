import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

let myData = {
  name: 'Adam',
  weather: 'sunny'
};

console.log('Hello ' + myData.name + '.');
console.log('Today is ' + myData.weather + '.');

let myData2 = {
    name2: 'Billy',
    weather2: 'cloudy',
    // tslint:disable-next-line: label-position
    printMessages: function() {
      console.log('Hello ' + this.name2 + '.');
      console.log('Today is ' + this.weather2 + '.');
    }
};
myData2.printMessages();

class MyClass {
  name3: any;
  weather3: any;

  constructor(name3, weather3) {
    this.name3 = name3;
    this.weather3 = weather3;
  }

  printMessages() {
    console.log('Hello ' + this.name3 + '.');
    console.log('Hello ' + this.weather3 + '.');
  }
}

const myData3 = new MyClass('Fred', 'windy');
myData3.printMessages();


class myClass4 {
      name4: any;
      weather4: any;

  constructor(name4, weather4) {
    this.name4 = name4;
    this.weather4 = weather4;
  }

  set weather(value) {
    this.weather4 = value;
  }

  get weather() {
    return `Today is ${this.weather4}`;
  }
  printMesages() {
    console.log('Hello ' + this.name4 + '.');
    console.log(this.weather4);
  }
}

let myData5 = new myClass4('Helen', 'nuclear fallout');
myData5.printMesages();


class MyClass5 {
    name5: any;
    weather5: any;
    
    constructor(name5, weather5) {
      this.name5 = name5;
      this.weather5 = weather5;
    }
    set weather(value1) {
      this.weather5 = value1;
    }
    get weather() {
      return `Today is ${this.weather5}`;
      }
      printMessages() {
        console.log("Hello " + this.name5 + ". ");
        console.log(this.weather5);
      }
}
      class MySubClass extends MyClass {
        city: any;

          constructor(name5, weather5, city) {
            super(name5, weather5);
            this.city = city;
          }
            printMessages() {
              super.printMessages();
              console.log(`You are in ${this.city}`);
            }
          }
    let myData6 = new MySubClass("Adam", "sunny", "London");
    myData6.printMessages();


import { Name, WeatherLocation } from './modules/NameAndWeather';
import { Name as OtherName } from './modules/DuplicateName';
import { TempConverter } from './modules/tempConverter';

    let name = new Name('Frank', 'Joji');
    let loc = new WeatherLocation('nuclear fire', 'Tokoyo');
    let other = new OtherName();

    let cTemp = TempConverter.convertFtoC(38);

    console.log(name.nameMessage);
    console.log(loc.weatherMessage);
    console.log(other.message);
    console.log(`The temp is ${cTemp}C`);





