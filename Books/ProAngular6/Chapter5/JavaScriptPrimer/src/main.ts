import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

console.log('Hello');
console.log('Apples');
console.log('This is a statement');
console.log('This is also a statement');


// tslint:disable-next-line: only-arrow-functions
const myFunc = function(name, weather, ...extraArgs) {
  
  console.log('Hello ' + name + '.');
  console.log('It is ' + weather + 'today.');
// tslint:disable-next-line: prefer-for-of
  for (let i = 0; i < extraArgs.length; i++) {
    console.log('Extra Arg: ' + extraArgs[i]);
  }
};

myFunc('Adam', 'sunny', 'one', 'two', 'three', 'four');

// tslint:disable-next-line: only-arrow-functions
const myFunc2 = function(name) {
  return ('Hello ' + name + 'How goes it with you?');
};

console.log(myFunc2('Adam'));

// tslint:disable-next-line: only-arrow-functions
const myFunc3 = function(nameFunction) {
  return ('Hello ' + nameFunction() + ' How goes it with you?');
};

// tslint:disable-next-line: only-arrow-functions
console.log(myFunc3(function() {
  return 'Sam';
}));

const myFunc4 = function(nameFunction) {
  return ('Hello' + nameFunction() + '.');
};

// tslint:disable-next-line: only-arrow-functions
const myFuncWeather = function(name) {
// tslint:disable-next-line: prefer-const
  let myLocalVar = 'sunny';
// tslint:disable-next-line: only-arrow-functions
  const innerFunction = function () {
    return ('Hello ' + name + '. Today is ' + myLocalVar + '.');
  }
  return innerFunction();
};
console.log(myFuncWeather('Adam'));

let messageFunction = function (weather) {
  let message = `It is ${weather} today`;
  console.log(message);
}

messageFunction('raining');

let name = 'Slore';

if (name == 'Adam') {
  console.log('Name is Adam');
} else if ( name == 'Jacqui') {
  console.log('Name is Jacqui');
} else {
  console.log('Name is neither Adam or Jacqui');
}

switch(name) {
  case 'Adam':
    console.log('Name is Adam');
    break;
  case 'Jacqui':
    console.log('Name is Jacqui');
    break;
  default:
    console.log('Name is neither Adam or Jacqui');
    break;
}

let firstVal = 5;
let secondVal = 5;

if (firstVal == secondVal) {
console.log('They are the same');
} else {
console.log('They are NOT the same');
}

let firstVal2 = 5;
let secondVal2 = '5';

if (firstVal2 === secondVal2) {
  console.log('They are the same');
} else {
  console.log('They are NOT the same');
}

let myData1 = 5 + 5;
let myData2 = 5 + '5';

console.log('Result 1: ' + myData1);
console.log('Result 2: ' + myData2);

let myData3 = (5).toString() + String(5);
  console.log('Result: ' + myData3);

let thirdVal = "5";
let fourthVal = "5";
let result = Number(thirdVal) + Number(fourthVal);
  console.log('Result: ' + result);

// tslint:disable-next-line: prefer-const
let myArray = new Array();
myArray[0] = 100;
myArray[1] = 'Adam';
myArray[2] = true;
myArray[3] = 'fifteen';
  console.log(myArray);
  console.log('Index of 0 ' + myArray[0]);

for (let i = 0; i < myArray.length; i++) {
    console.log('Index ' + i + ':' + myArray[i]);
}

myArray.forEach((value, index) => console.log('Index ' + index + ': ' + value));

let otherArray = [...myArray, 200, 'Bob', false];
  for (let i = 0; i < otherArray.length; i++) {
  console.log(`Array item ${i}: ${otherArray[i]}`);
}

let products = [
  { name: "Hat", price: 24.5, stock: 10 },
  { name: "Kayak", price: 289.99, stock: 1 },
  { name: "Soccer Ball", price: 10, stock: 0 },
  { name: "Running Shoes", price: 116.50, stock: 20 }
  ];
  let totalValue = products
  .filter(item => item.stock > 0)
  .reduce((prev, item) => prev + (item.price * item.stock), 0);
    console.log("Total value: $" + totalValue.toFixed(2));











