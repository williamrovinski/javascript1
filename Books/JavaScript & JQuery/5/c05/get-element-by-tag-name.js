var elements = document.getElementsByTagName('li');     // Find <li> elements 

if (elements.length > 3) {                              /* If 1 or more are found, just make sue anything you put the elements.length is 
                                                            less than the actual number of items you have in your list.
															I believe it is lists that count 1-2-3-4, while arrays count 0-1-2-3, etc.
															In this case I select greater than 3, since the list item is 4 or 
															higher(greater than 3).*/

    var el = elements[3];                               /* Select the first one using array syntax, in this case I chose the 3rd value 
	                                                       in the array, which is item 4.*/
	el.className = 'cool';                              // Change the value of the class attribute 

}