/* Store the first list item in a variable, it seems as though when I edit the document I can't update the innerHTML
   without every other element ID taking the value of the highest element ID. Need to find a way to have each element
   be crossed out.*/
   
var firstItem = document.getElementById('one');

var secondItem = document.getElementById('two');

var threeItem = document.getElementById('three'); 

//Get the content of the first list item
var itemContent = firstItem.innerHTML;

var itemContent = secondItem.innerHTML;

var itemContent = threeItem.innerHTML;

//Update the content of the first list item so it is a link 
firstItem.innerHTML = '<a href=\"https://giantfoodstores.com\">' + itemContent + '</a>'; // By changing the a to an s, you see that 
                                                                                         // instead of a link it shows as crossed off

secondItem.innerHTML = '<s href=\"https://giantfoodstores.com\">' + itemContent + '</a>';

threeItem.innerHTML = '<s href=\"https://giantfoodstores.com\">' + itemContent + '</a>';

  
