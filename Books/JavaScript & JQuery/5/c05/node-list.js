var hotItems = document.querySelectorAll('li.hot');   // Store NodeList in array

if (hotItems.length > 2) {                            /* If it contains items, remember you have 3 hot items so if hotItems.length is greater 
                                                         than 3 or your hot items the loop will be null but any number less than 3 will 
														 make it loop through the entire set. In this case I wrote 2. */
    for (var i=1; i<hotItems.length; i++) {           /* Loop through each item, remember it counts the array 0,1,2,3. 
	                                                     So fresh figs won't be looped through. */
	     hotItems[i].className = 'cool';              // Change value of class attribute
	}
	
}