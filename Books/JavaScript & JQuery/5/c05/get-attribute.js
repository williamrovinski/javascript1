var firstItem = document.getElementById('one');        //Get first list item
var secondItem = document.getElementById('four');

if (firstItem.hasAttribute('class')) {                //If it has class attribute
    var attr = firstItem.getAttribute('class');       //Get the attribute
	var attr = secondItem.getAttribute('class');

	
	//Add the value of the attribute after the list 
	var el = document.getElementById('scriptResults');  /*<----See that 'scriptResults' don't worry about it, it won't 
	                                                           affect your JavaScript code itself, but those
	                                                           things in single bracket quotations are reffering back 
															   to your HTML as a reference, if not defined on your html
															   then javascript will run an error on your browser. */
	el.innerHTML = '<p>The first item has a class name: ' + attr + '</p>'; 

}