// querySelector() only returns the first match 
var el = document.querySelector('li.hot');    /* The only way for you to select more than one element is a query selector, because
                                                 as far as I know its the only one where you would enter 
												 var els = document.querySelector('li.hot'); <- els is multible elements.
												 els[1].className = 'cool'; <- els down here too. Remember its a class your selecting under
												 list because nothing else you have describes multible elements other than classname hot.
												 */
el.className = 'cool';

// querySelectorAll returns a NodeList 
// The second matching element (the third list item) is selected and changed
var els = document.querySelectorAll('li.hot');
els[0].className = 'cool'; 
