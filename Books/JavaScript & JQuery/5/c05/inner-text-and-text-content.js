var firstItem = document.getElementById('one');       //Find first list item
var showTextContent = firstItem.textContent;          //Get value of textContent
var showInnerText = firstItem.innerText;              //Get value of innerText

var secondItem = document.getElementById('two');      //Find second list item 
var showTextContent = secondItem.textContent;         //Get value of textContent
var showInnerText = secondItem.innerText;             //Get value of innerText


//Show the content of these two properties at the end of the list
var msg = '<p>textContent: ' + showTextContent + '</p>';
    msg += '<p>innerText: ' + showInnerText + '</p>';
var el = document.getElementById('scriptResults');
el.innerHTML = msg;

var msg = '<p>textContent: ' + showTextContent + '</p>';
    msg += '<p>innerText: ' + showInnerText + '</p>';
var el = document.getElementById('scriptResults2');
el.innerHTML = msg;

firstItem.textContent = 'sourdough bread';            //Update the first line item

secondItem.textContent = 'grape juice';  