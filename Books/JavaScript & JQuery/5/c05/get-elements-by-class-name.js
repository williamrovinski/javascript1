/*Selecting a DOM tree element from the html file requires no variable
  defined previously, you're simply getting the var element by typing in a .getElementByClassName(); 
  .getElementsByTagName(); querySelectorAll(); */  

var elements = document.getElementsByClassName('hot'); // Find hot items

if (elements.length > 2) {                            // If 3 or more are found, we have 3 items that have a class name of 'hot'. 

  var el = elements[1];              /* Select the third one[2] from the NodeList; this appears to be the only part that changes the
                                        element in which you want changed, 0 is the 1st item at which most arrays start. In this case I chose 
										the value of the 2nd item.*/  
  el.className = 'cool';             // Change the value of its class attribute

}