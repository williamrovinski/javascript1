var msg = 'Sign up to recieve our newsletter for 10% off!';

/* The function begins with you declaring the variable, next name your function
   and you'll see how the function has a condition denoted with () and a statement denoted 
   with {}. 
*/  
function updateMessage () {
  var el = document.getElementById('message');
  el.textContent = msg;
}
updateMessage(); 
