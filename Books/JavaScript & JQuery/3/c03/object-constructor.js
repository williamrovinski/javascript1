// This variable is what sets up the object seen as checkAvailability, think of an object as a globally defined function within a variable. 
var hotel = new Object();

hotel.name = 'Park';
hotel.rooms = 120;
hotel.booked = 77;
hotel.checkAvailability = function() {
  return this.rooms - this.booked;  
};

//This will update the html
var elName = document.getElementById('hotelName'); //gets your element  from our variable contained within var hotel. 
elName.textContent = hotel.name;                   //Update html with property of this object 

var elRooms = document.getElementById('rooms');   //gets your element  from our variable contained within var hotel. 
elRooms.textContent = hotel.checkAvailability();   // Update HTML with property of this object 