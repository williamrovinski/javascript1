// This variable is what sets up the object seen as checkAvailability, think of an object as a globally defined function within a variable. 

var hotel = {
  name: 'Quay',
  rooms: 40,
  booked: 25,
  checkAvailability: function() {
    return this.rooms - this.booked; // This goes inside the function so it can be returned later 
  }
};

//This will update the html
var elName = document.getElementById('hotelName'); //gets your element  from our variable contained within var hotel. 
elName.textContent = hotel.name;                   //Update html with property of this object 

var elRooms = document.getElementById('rooms');   //gets your element  from our variable contained within var hotel. 
elRooms.textContent = hotel.checkAvailability();   // Update HTML with property of this object 
