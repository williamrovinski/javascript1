  var xhr = new XMLHttpRequest();              //Create XMLHttpRequest object

  xhr.onload = function() {                    //When response has loaded
   // The following conditional check will not work locally - only on a server 
   if(xhr.status === 200) {                  //If server status was ok
     document.getElementById('content').innerHTML = xhr.responseText;/* This will find
                                                                        the id 'content'
																		and update it
																		what does the xhr
																		do? it triggers a response.*/
   }
  };
  
  xhr.open('GET', 'data/data.html', true);   /*Prepare the request, it almost looks like a 
                                               an event method construction.*/  
  xhr.send(null);                            //Send the request
  