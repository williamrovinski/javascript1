var score1 = 90;        //Round 1 score 
var score2 = 95;        //Round 2 score
var highScore1 = 75;    //Round1 in high score
var highScore2 = 95;    //Round 2 in high score 

// Check if scores are higher than current high scores, this is true 
var comparison = (score1 + score2) > (highScore1 + highScore2);

/* Check some other scores, this is a greater than function, 
   it'll be false since 165 isn't greater than 190. */
var comparison2 = (score1 + highScore1) > (score2 + highScore2);

// Final attempt, this is a strictly non equal equation, so it'll return true
var comparison3 = (score2 + highScore1) !== (score1 - highScore1);



// Write the message into the page 
var el = document.getElementById('answer');
el.textContent = 'New high score: ' + comparison; 

var el = document.getElementById('answer2');
el.textContent = 'Newer high score: ' + comparison2;

var el = document.getElementById('answer3');
el.textContent = 'Newest high score: ' + comparison3;
