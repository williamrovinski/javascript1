var i = 1; 
var msg = ''; 

// Store 5 times table in a variable 
while (i < 10) {
  // As you can see we have i = 1, then it adds 1+1 each time until i < 10 or 9.
  // So the first block of line 1 is i +, 5=, then i * 5 is what the thing will be equal to. 
  // It looks like 1 x 5 = 5, 2 x 5 = 10, and so on.   
  msg += i + ' x 5 = ' + (i * 5) + '<br />';
  i++;
}

document.getElementById('answer').innerHTML = msg; 