$(function() {
/* The first statement finds the third list item and removes hot from the 
   class attribute on that element attribute on that element. This is important 
   to note because it affects the next statement. */ 
  $('li#three').removeClass('hot');
/* The second statement selects all <li> elements whose class attribute has a value of hot
   It adds a new class name called favorite. Because step 1 upldated the third list item, 
   this statement affects only the first two.*/  
  $('li.hot').addClass('favorite');
/* The third statement selects the <ul> element and adds an id attribute, giving it a 
   value of group.*/
  $('ul').attr('id', 'group');
});
