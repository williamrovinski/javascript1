/* The selector returns the <ul> element. The .text() method gets the text from all of the <ul> 
   element's children. This is then appended to the end of the selection, in this case 
   after the existing <ul> element.*/

var $listText = $('ul').text();
var $listText2 = $('h2').text();
$('ul').append('<p>' + $listText + '</p>');
$('h2').append('<h2>' + $listText2 + '</h2>').addClass('cool').hide().delay(500).fadeIn(1400); 
$('h2').append('<h2>' + $listText2 + '</h2>').addClass('cool').hide().delay(550).fadeIn(1450); 