$(function() {
/* The backgroundColor variable is created. The jQuery selection contains all <li> elements, and
   the .css() method returns the value of the background-color property of the first list item.*/	
  var backgroundColor = $('li').css('background-color');
/* The background color of the first list item is written into the page using the .append()
   method. Here, it is used to add content after the <ul> element.*/ 
  $('ul').append('<p>Color was: ' + backgroundColor + '</p>');
/* The selector picks all <li> elements, and then the .css() method updates several properties 
   at the same time. From lines 10-21*/  
  $('li').css({
	 //The background color is changed to brown. 
    'background-color': '#c5a996',
	//Border changed to white.
	'border': '1px solid #fff',
	//The color of the text is changed to black.
	'color': '#000',
	//The typeface is changed to Georgia.
	'font-family': 'Georgia',
	//Extra padding is added on the left.
	'padding-left': '+=75'
  });
});