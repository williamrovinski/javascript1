/* The first line selects all of the <h1>-<h6> headings, and adds a value of headline to their class
   notice that if you look at the plain c06.css you won't see healine class anywhere, thats because 
   Jquery created it by that line of code*/
$(':header').addClass('headline');
/* The second line selects the first three list items and does two things:
   1. The elements are hidden.
   2. The elements fade into view. */
$('li:lt(3)').hide().fadeIn(1400);
/* From lines 8-13, the last three lines of the script set an event listener on each of the <li> elements.
   When a user clicks on one, it triggers an anonymous function to remove that element from the page.*/
$('li').on('click', function() {
  $(this).remove();
});