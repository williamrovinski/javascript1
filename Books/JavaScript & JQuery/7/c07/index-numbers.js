$(function() {
  $('li:lt(2)').removeClass('hot');
  $('li').eq(0).addClass('complete');
  $('li:gt(3)').addClass('cool');
});

/* When doing this remember the id="one" or whatever is irrelevant what is relevant is the order, list starts at zero and goes till infinity. */ 