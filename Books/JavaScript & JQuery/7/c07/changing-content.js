$(function() {
/* This line selects any list items that contain the word pine. 
   It then changes the text of the matching element to almonds using the .text() method.*/	
 $('li:contains("pine")').text('almonds');
/* These lines select all list items whose class attributes contains the word hot, and uses the
   .html() method to update the content of each of them.
   
   The .html() method uses a function to place the content of each element inside an <em> element
   inside an <em> element.*/
 $('li.hot').html(function() {
   return '<em>' + $(this).text() + '</em>';
 });
/* This line selects the <li> element that has an id attribute whose value is one, 
   then uses the remove() method to remove it.
   
   When specifying new content, carefully choose when to use single quotes and when to use
   double quotas. If you append a new element that has attributes, use single quotes to surround 
   the content. Then use double quotes for the attributes values themselves.*/ 
 $('li#one').remove();
});
