/* The selector returned the <ul> element. The html() method gets all the HTML inside it(the four 
   <li> elements). This is then appended to the end of the selection, 
   in this case after the existing <li> elements. */

var $listHTML = $('ul').html();
$('ul').append($listHTML); //This is much easier than how I did it with pure JavaScript