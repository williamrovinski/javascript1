var $listItems = $('li');
/* The /filter() methods finds the last list item with a class attribute whose value is hot. li then removes that value from the class attribute.*/
$listItems.filter('.hot:last').removeClass('hot');
/* The :not() selector is used within the jQuery selector to find <li> elements without a value of hot in their class attribute and adds a value of cool.*/ 
$('li:not(.hot)').addClass('cool');
/* The .has() method finds the <li> element that alos contains an <em> element within it and adds the value complete to the class attribute.*/
$listItems.has('em').addClass('complete');
$listItems.filter('.hot:first').removeClass('hot');
/* The .each() method loops through the list items. The current element is cached in a jQuery object. The .is() method looks to see if the <li> 
   element has a class attribute whose value is hot. If it does, 'Priority item: ' is added to the start of the item.*/ 
$listItems.each(function() {
 var $this = $(this);
 if ($this.is('.hot')) {
   $this.prepend('priority item: ');
 }
});
/* The :contains selector checks for <li> elements that contain the text "honey" and appends the text " (local)" to the end of those items.*/
$('li:contains("honey")').append(' (local)'); 

//Remember for honey and salmon, they are no longer priority items because either the hot class was removed or replaced with another class of cool or nothing. 
