$(function() {

  $('li').on('dblclick', function(e) {
/* Any <span> elements that already exist inside the <li> elements are removed.
   In fact if you added a span class of any sort, when clicked the text within the
   span class will be removed. of course if you have text outside of the span ids
   while on the same line that text will remain.*/
    $('li span').remove();
/* A new Date object is created, and its time is set to the time at which the event was clicked.*/
	var date = new Date();
	date.setTime(e.timeStamp);
/* The time the event was clicked is then converted into a date that can be read.*/
	var clicked = date.toDateString();
	$(this).append('<span class="date">' + clicked + ' ' + e.type + '</span>');
  });
  
});
