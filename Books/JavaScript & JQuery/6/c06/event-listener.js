function checkUsername() {                                       // Declare function
  var elMsg = document.getElementById('feedback');               // Get feedback element 
  if (this.value.length < 5) {                                  // If username too short
    elMsg.textContent = 'Username must be 5 characters or more'; // Set msg
  } else {                                                       // Otherwise
    elMsg.textContent = 'Looks like its a match';                                      // Clear msg
  }
}

var elUsername = document.getElementById('username');            // Get username input, this is the line of code that makes feedback run under 'username'.   
// When it loses focus call checkUsername()
elUsername.addEventListener('blur', checkUsername, false);      //Here is the event-listener, the event is 'blur', the code or function is checkUsername.  and finally 
                                                                //The false is the boolean. 
  