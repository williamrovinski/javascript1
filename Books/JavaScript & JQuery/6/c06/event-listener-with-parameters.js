var elUsername = document.getElementById('username');        //Get username input
var elMsg = document.getElementById('feedback');             //Get feedback element

function checkUsername(minLength) {                          //Declare function
  if (elUsername.value.length < minLength) {                //If username too short 
  // Set the error message
  elMsg.textContent = 'Username must be ' + minLength + ' characters or more';
 } else {                                                    //Otherwise
  elMsg.innerHTML = '';                                      //Clear msg
 }
}
//This basically shoves a function inside an event method which allows you to add parameters. Otherwise it looks like any other event 
elUsername.addEventListener('blur', function() {              /*This is one big element and method, but lets look at that further,  
                                                                This is when the anonymus function is used as the secod arguement. 
																It wraps around the named function. */
                                                              /*function checkUsername, listed on line 4 id the named function which includes the parenthesis giving us 
															    the parameters of it. In this case its 5. */
  checkUsername(5);                                           // The parameters 5 and we could make it anything    
}, false);                                                    //Pass arguements here 