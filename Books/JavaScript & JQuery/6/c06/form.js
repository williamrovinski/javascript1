var elForm, elSelectPackage, elPackageHint, elTerms;     // Declare variables
elForm          = document.getElementById('formSignup'); // Store elements 
elSelectPackage = document.getElementById('package');   
elPackageHint   = document.getElementById('packageHint');
elTerms         = document.getElementById('terms');
elTermsHint     = document.getElementById('termsHint'); 

function packageHint() {                                 //Declare function
  var package = this.options[this.selectedIndex].value;  //Get selected option
  if (package == 'monthly') {                            /*If monthly package, remember if you're starting and if, else if, else statement. 
                                                           That you need to set the condiion equal to something before declaring the function
                                                           itself. Typically, when making an these chains, you start with an if, then else if,
														   you can have as many as you need, but the final statement of the chain must be else. */														   
    elPackageHint.innerHTML = 'Save $10 if you pay for 1 year!'; //Show this msg
  } else if (package == 'annual') {                                               //Otherwise
    elPackageHint.innerHTML = 'Wise choice!';            //Show this message
  } else if (package == 'three') {
    elPackageHint.innerHTML = 'Wow';
  } else {
	elPackageHint.innerHTML = "That's some dedication there";
  }
 }
 
 function checkTerms(event) {                            //Declare function
   if (!elTerms.checked) {                               //If checkbox ticked
     elTermsHint.innerHTML = 'You must agree to the terms.'; //Show message
	 event.preventDefault();                             //Don't submit form 
   }
 }
 
 //Create event listeners: submit calls checkTerms(), change calls packageHint()
 elForm.addEventListener('submit', checkTerms, false);
 elSelectPackage.addEventListener('change', packageHint, false); 