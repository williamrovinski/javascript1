/* Create variables for the welcome message, keep in mind all these variable you're making are global in scope, 
   for a simple project such as this. */  
var greeting = 'Howdy ';
var name = 'Molly';
var message = ', please check your order:';

/* Concatenate the three variables above to create the welcome message, 
   by that it means, you've defined three variables now you can combine all three 
   into one variable you can get returned to you. */    
var welcome = greeting + name + message;

// Create variables to hold details about the sign, this will be the center part in div content. 
var sign = 'Montague House';
var tiles = sign.length;
var subTotal = tiles * 5;
var shipping = 7;
var grandTotal = subTotal + shipping;

/* Get the element that has an id of greeting, doing this will return the welcome 
   message where you ave the greeting string selected. */
var el = document.getElementById('greeting');
/* Replace the content of that element with the personalized message, this will replace the 
   string 'greeting' div id with var welcome */
el.textContent = welcome;

var elSign = document.getElementById('userSign');
elSign.textContent = sign;

var elTiles = document.getElementById('tiles');
elTiles.textContent = tiles;

var elSubTotal = document.getElementById('subTotal');
elSubTotal.textContent = '$' + subTotal;

var elShipping = document.getElementById('shipping');
elShipping.textContent = '$' + shipping;

var elGrandTotal = document.getElementById('grandTotal');
elGrandTotal.textContent = '$' + grandTotal;

