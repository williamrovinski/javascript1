(function(){
  'use strict'; 
  
  var newElement = document.createElement('li');
  newElement.textContent = "I am a new order listed Element"; 
  
  var list = document.getElementById('ol');
  //list.appendChild(newElement);
  
  list.insertBefore(newElement,list.firstElementChild);
  
  console.log(newElement); 
}());