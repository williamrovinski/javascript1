const cards = document.querySelectorAll('.memory-card');

let hasFlippedCard = false;
let lockBoard = false; 
let firstCard, secondCard;

function flipCard() {
    if(lockBoard) return; 

    this.classList.add('flip'); 

    if (!hasFlippedCard) {
        // first click
        hasFlippedCard = true;
        firstCard = this;

        return;

    } else {
        // second click
        hasFlippedCard = false;
        secondCard = this; 

        checkForMatch(); 
    }
}

function checkForMatch(){
    let isMatch = firstCard.dataset.framework === secondCard.dataset.framework;  

    isMatch ? disableCards() : unflipCards();
}

function disableCards() {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);

    resetBoard(); 
}

function unflipCards() {
    lockBoard = true;

    setTimeout(() => {
        firstCard.classList.remove('flip');
        secondCard.classList.remove('flip');

    lockBoard = false; 
    }, 2000);
}

function resetBoard() {
    [hasFlippedCard, lockBoard] = [false, false];
    [firstCard, secondCard] = [null, null]; 
}

(function shuffle() {
    cards.forEach(card => {
        let randomPos = Math.floor(Math.random() * 24);
        card.style.order = randomPos; 
    });
})();


cards.forEach( card => card.addEventListener('click', flipCard));

/*function flipCard() {
    this.classList.toggle('flip'); /*toggle means if class or thing is present change it to that.
                                    in this case we have flip which means it'll look for memory-card.flip and
                                    it knows flip is concatenated on the end so it'll add it.
                                    if you made the concatenated class named anything else then it won't toggle the class.
}*/