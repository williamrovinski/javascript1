function findBiggestFraction(a,b) {
    a>b ? console.log("a: ", a) : console.log("b: ", b);
}

var firstFraction = 3/4;
var secondFraction = 5/7;

findBiggestFraction(firstFraction,secondFraction);
findBiggestFraction(7/16,13/25);
findBiggestFraction(1/2,3/4);
findBiggestFraction(5/12,8/45);
findBiggestFraction(10/120,15/38);
findBiggestFraction(7/16,13/25);
findBiggestFraction(1/2,3/4);
findBiggestFraction(5/12,8/45);
findBiggestFraction(10/120,15/38);